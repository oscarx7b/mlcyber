# Machine Learning for Cyber Security.

This project contains the Jupyter notebooks for ML4Cyber.

There are 3 possible ways to run the notebooks,
- Host own container
- Run in Binder
- Run in Google Coblaboratory

## Pros and Cons

We decided on using Binder adn its simple and free to setup. It generates URL from github/gitlab repo that can shared with anyone to replicate the notebook.

### Host own Container

(+) Can easily manage by ourselves using dockerfile to add files

(+) Codes and files can be kept private

(-) Need use AWS infra with slightly higher vcpu and mem = more costs

### Binder

(+) Should be relatively free

(-) Need to have public github/gitlab repo to pull

### Google Colaboratory

(+) Already running jupyter notebook hosted by google

(-) Need to login to google

